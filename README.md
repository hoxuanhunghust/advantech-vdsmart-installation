1. Nvidia driver version: Software & Update > Addtional Driver >  nvidia-driver-465 > Apply > Restart

2. Install Ananconda:
```
wget https://repo.anaconda.com/archive/Anaconda3-2021.05-Linux-x86_64.sh
bash Anaconda3-2021.05-Linux-x86_64.sh
conda create -n vdsmart python=3.6.5
conda activate vdsmart
echo 'conda activate vdsmart' >> ~/.bashrc 
```

3. Install cuda
```
sudo apt install -y cuda-10.0
```

4. Install cudnn

Extract cudnn-10.2-linux-x64-v8.0.5.39.tgz

```
sudo cp include/cudnn.h /usr/local/cuda-10.0/include
sudo cp lib64/libcudnn* /usr/local/cuda-10.0/lib64
sudo chmod a+r /usr/local/cuda-10.0/lib64/libcudnn*

echo 'export PATH=/usr/local/cuda-10.0/bin${PATH:+:${PATH}}' >> ~/.bashrc 
echo 'export LD_LIBRARY_PATH=/usr/local/cuda-10.0/lib64/{LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}' >> ~/.bashrc 

```

5. Install Library
```
pip install -r requirement.txt
pip install torch==1.7.1+cu101 torchvision==0.8.2+cu101 torchaudio==0.7.2 -f https://download.pytorch.org/whl/torch_stable.html
```
<i>To test Tensorflow please run: 'python test_tf.py'</i>

6. Install Nvidia-docker:
https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html


